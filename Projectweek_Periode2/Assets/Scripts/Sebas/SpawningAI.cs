﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawningAI : MonoBehaviour {


    private Transform _spawnPoint;

    private Transform ObjectSpawn;


    private float SpeedProjectile = 500f;

    private float SpawnTime = 3f;
    private float Timer = 0f;

    private Transform _target;
    private Transform _objective;

    public void SetTargetAtStart(Transform target)
    {
        _target = target;
    }

    public void SetObjectiveAtStart(Transform objective)
    {
        _objective = objective;
    }

    public void SetSpawnDelay(float delay)
    {
        SpawnTime = delay;
    }

    // Use this for initialization
    void Start ()
    {
        ObjectSpawn = Resources.Load<GameObject>("Ayoub/Enemy").GetComponent<Transform>();

        _spawnPoint = this.transform;
    }
	
	// Update is called once per frame
	void Update () {
        Timer += 1f * Time.deltaTime;

        if (Timer >= SpawnTime)
        {
            Spawn();
            Timer = 0;
        }
    }

    private void Spawn()
    {
        Transform clone = Instantiate(ObjectSpawn, _spawnPoint.position, _spawnPoint.rotation);
        clone.GetComponent<AIController>()._objective = _objective;
        clone.GetComponent<AIController>()._target = _target;
    }
}
