﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerHealth : MonoBehaviour
{


    [SerializeField]
    private Slider m_SliderHealth;

    private float m_Damage = 0.001f;
    private float m_DistanceTowerEnemy;
    [SerializeField]
    private Transform Enemy, Tower;

    


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        m_DistanceTowerEnemy = Vector3.Distance(Tower.position, Enemy.position);
        if (m_DistanceTowerEnemy <= 5)
        {
            m_SliderHealth.value += m_Damage;
        }        
    }    
}
