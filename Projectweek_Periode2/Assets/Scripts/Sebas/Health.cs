﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

    [SerializeField] private float _maxHealt = 100f;
    [SerializeField] private float _currentHealt = 0f;

    [SerializeField] GameObject explosion;

    private void Start()
    {
        _currentHealt = _maxHealt;
    }

    public void DoDamage(float damage)
    {
        _currentHealt -= damage;

        if (_currentHealt <= 0f)
        {
            Die();
        }
    }   

    private void Die()
    {
        GameObject clone = Instantiate(explosion, transform.position, transform.rotation);
        Destroy(this.gameObject);
    }
}
