﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    private Camera m_Camera;
    float TheDammage = 25f;

    private float _fireDelay = 2.5f;
    private float _countDown = 0f;

    RaycastHit hit;

    [SerializeField]
    private GameObject m_ParticleBullet;

    [SerializeField]
    private Transform _barrelEnd;

    private AIController _enemyController;

    public Animator _arms;
    public Animator _gun;

    void Start()
    {
        if (transform.CompareTag("Enemy"))
        {
            _enemyController = GetComponent<AIController>();
            TheDammage = 7f;
            _countDown = _fireDelay;
        }
        else
        {
            m_Camera = transform.GetComponentInChildren<Camera>();
        }
    }

    void Update()
    {
        if (transform.CompareTag("Enemy"))
        {
            if (_enemyController._canSeeTarget)
            {
                AIFire();

                _countDown += 1f * Time.deltaTime;
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                PlayerFire();
            }
        }
    }

    private void DoParticle()
    {
        GameObject clone = Instantiate(m_ParticleBullet, _barrelEnd.position, _barrelEnd.rotation);
    }

    private void PlayerFire()
    {
        if (Physics.Raycast(m_Camera.transform.position, m_Camera.transform.forward * 100f, out hit, 100f))
        {
            if (hit.transform.CompareTag("Enemy"))
            {
                hit.transform.gameObject.GetComponent<Health>().DoDamage(TheDammage);
            }
        }

        _arms.SetTrigger("Shoot");
        _gun.SetTrigger("shoot");
        DoParticle();
    }

    private void AIFire()
    {
        if(_countDown < _fireDelay)
        {
            return;
        }
        else
        {
            if (Physics.Raycast(_barrelEnd.transform.position, _barrelEnd.transform.parent.forward * 100f, out hit, 100f))
            {
                if (hit.transform.CompareTag("Player"))
                {
                    hit.transform.gameObject.GetComponent<Health>().DoDamage(5f);
                }
            }

            _countDown = _fireDelay;
        }
    }
}