﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;


[RequireComponent(typeof(NavMeshAgent))]
public class FOV : MonoBehaviour {

    // Variables FOV
    public float ViewRadius;
    [Range(0, 360)]
    public float ViewAngle;

    // Variables AI
    [SerializeField]
    public Transform Target;
    public bool _canSeeTarget { get; internal set; }

    [SerializeField]
    private Transform Objective;

    private NavMeshAgent NavAgent;

    private Vector3 DirectTarget;

    private RaycastHit Hit;

    public Vector3 DirFromAngle(float AngleInDegrees, bool AngleIsGlobal)
    {
        if (!AngleIsGlobal)
        {
            AngleInDegrees += transform.eulerAngles.y;
        }

        return new Vector3(Mathf.Sin(AngleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(AngleInDegrees * Mathf.Deg2Rad));
    }

    void Start()
    {
        NavAgent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        DirectTarget = (Target.position - transform.position).normalized;

        // When the target isn't in the FOV
        NavAgent.SetDestination(Objective.position);

        // When the target is in the FOV
        if (Physics.Raycast(transform.position, DirectTarget, out Hit, ViewRadius))
        {
            if(Vector3.Angle(transform.forward, DirectTarget) < ViewAngle / 2)
            {
                NavAgent.SetDestination(Target.position);
                _canSeeTarget = true;
            }
            _canSeeTarget = false;

            Debug.DrawLine(transform.position, Hit.point, Color.green);

            //// When the enemy sees the player
            //if (Hit.collider.name == "Player")
            //{
            //    NavAgent.SetDestination(Target.position);
            //}
            //Debug.DrawLine(transform.position, Hit.point, Color.green);
        }
    }
}
