﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]

public class Walk : MonoBehaviour
{
    [SerializeField]
    public Transform Target;

    [HideInInspector]
    public bool _canSeeTarget { get; internal set; }

    [SerializeField]
    private Transform Objective;

    private NavMeshAgent NavAgent;
    private Vector3 DirectTarget;
    private RaycastHit Hit;

    // Use this for initialization
    void Start()
    {
        NavAgent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        DirectTarget = (Target.position - transform.position).normalized;

        // When the enemy doesn't see the player
        NavAgent.SetDestination(Objective.position);

        if (Physics.Raycast(transform.position, DirectTarget, out Hit, 15f))
        {
            // When the enemy sees the player
            if (Hit.collider.name == "Player")
            {
                _canSeeTarget = true;
                NavAgent.SetDestination(Target.position);
            }

            else
            {
                _canSeeTarget = false;
            }

            Debug.DrawLine(transform.position, Hit.point, Color.green);
        }
        else
        {
            Debug.DrawRay(transform.position, DirectTarget * 15f, Color.red);
            _canSeeTarget = false;
        }
    }
}