﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIController : MonoBehaviour
{
    //FOV Variables
    [Header("FOV Settings")]
    [SerializeField] private float _viewRadius = 10f;
    [SerializeField] [Range(0, 360f)] private float _viewAngle = 60f;

    //AI Variables
    [HideInInspector] public Transform _target;
    [HideInInspector] public Transform _objective;

    private Vector3 _directionToTarget = Vector3.zero;
    public bool _canSeeTarget { get; internal set; }

    private Vector3 _raycastOrigin;
    private Vector3 _directionToNextWaypoint;

    private CharacterController _controller;
    private RaycastHit _hit;
    private NavMeshPath _path;

    [Header("Movement Settings")]
    [SerializeField] private float _movementSpeed = 3f;
    [SerializeField] private float _rotationSpeed = 0.1f;

    private float _gravity = 14f;
    private float _verticalVelocity = 0f;

    private void Start()
    {
        _controller = GetComponent<CharacterController>();
        _path = new NavMeshPath();
    }

    private void Update()
    {
        if(_target == null)
        {
            _target = _objective;
        }

        _directionToTarget = _target.transform.position - transform.position;
        _directionToTarget.Normalize();

        _raycastOrigin = new Vector3(transform.position.x, transform.position.y + 1f, transform.position.z);

        DoRaycasts();
        CalculatePath();
        RotateToNextWaypoint();
        Move();
        DoDebug();
    }

    private void DoRaycasts()
    {
        if(Physics.Raycast(_raycastOrigin, _directionToTarget, out _hit, _viewRadius))
        {
            _canSeeTarget = (_hit.transform.CompareTag("Player") && (Vector3.Distance(transform.position, _target.position) <= _viewRadius));
        }
    }

    private void CalculatePath()
    {
            NavMesh.CalculatePath(transform.position, _target.position, -1, _path);
    }

    private void RotateToNextWaypoint()
    {
        _directionToNextWaypoint = _path.corners[1] - transform.position;
        _directionToNextWaypoint.y = 0f;
        _directionToNextWaypoint.Normalize();

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(_directionToNextWaypoint), _rotationSpeed);
    }

    private void Move()
    {
        //Jumping
        if (_controller.isGrounded)
        {
            _verticalVelocity = -_gravity * Time.deltaTime;
        }
        else
        {
            _verticalVelocity -= _gravity * Time.deltaTime;
        }

        _directionToNextWaypoint *= _movementSpeed;
        _directionToNextWaypoint.y = _verticalVelocity;

        _controller.Move(_directionToNextWaypoint * Time.deltaTime);
    }

    private void DoDebug()
    {
        //draw path
        for (int i = 0; i < _path.corners.Length - 1; i++)
        {
            Debug.DrawLine(_path.corners[i], _path.corners[i + 1], Color.red);
        }
    }
}