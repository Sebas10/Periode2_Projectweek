﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtPlayer : MonoBehaviour
{
    private bool _playerDetected;

    private GameObject _player;
    private Transform _guns;

    private FOV _aiScript;

    private void Start()
    {
        _aiScript = transform.parent.GetComponent<FOV>();
        _player = _aiScript.Target.gameObject;

        for(int i = 0; i < transform.childCount; i++)
        {
            if(transform.GetChild(i).name == "Guns Mesh")
            {
                _guns = transform.GetChild(i);
            }
        }
    }

    private void Update()
    {
        _playerDetected = _aiScript._canSeeTarget;

        if (_playerDetected)
        {
            Vector3 direction = new Vector3(0, _player.transform.position.y + 0.5f, _player.transform.position.z) - transform.position;
            RotateArms(Quaternion.LookRotation(direction));
        }
        else
        {
            RotateArms(Quaternion.identity);
        }
    }

    private void RotateArms(Quaternion rotation)
    {
        _guns.transform.localRotation = Quaternion.Lerp(_guns.transform.localRotation, rotation, 0.5f);
    }
}
