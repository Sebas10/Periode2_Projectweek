﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour
{
    [SerializeField] private float _walkSpeed = 3f;
    [SerializeField] private float _runSpeed = 5f;

    [SerializeField] private float _smoothTime = 0.2f;

    private float _currentSpeed = 0f;

    //Jumping
    private float _verticalVelocity = 0f;
    private float _gravity = 14.0f;
    private float _jumpForce = 4.0f;

    private Vector3 _moveDirection = Vector3.zero;
    private Vector3 _targetDirection = Vector3.zero;

    private CharacterController _controller;

    private void Start()
    {
        _controller = GetComponent<CharacterController>();
    }

    private void Update()
    {
        GetInput();
        Move();
    }

    private void GetInput()
    {
        if (_controller.isGrounded)
        {
            _targetDirection = transform.rotation * new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));
            _targetDirection.Normalize();

            if (Input.GetKey(KeyCode.LeftShift))
            {
                _currentSpeed = _runSpeed;
            }
            else
            {
                _currentSpeed = _walkSpeed;
            }

            _targetDirection *= _currentSpeed;

            _verticalVelocity = -_gravity * Time.deltaTime;
            if (Input.GetKeyDown(KeyCode.Space))
            {
                _verticalVelocity = _jumpForce;
            }
        }
        else
        {
            _verticalVelocity -= _gravity * Time.deltaTime;
        }
        
        _moveDirection.y = _verticalVelocity;
    }

    private void Move()
    {
        _moveDirection.x = Mathf.Lerp(_moveDirection.x, _targetDirection.x, _smoothTime);
        _moveDirection.z = Mathf.Lerp(_moveDirection.z, _targetDirection.z, _smoothTime);

        _controller.Move(_moveDirection * Time.deltaTime);
    }
}
