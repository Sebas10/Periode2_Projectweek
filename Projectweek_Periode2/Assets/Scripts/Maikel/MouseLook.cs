﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
    public RotationAxes axes = RotationAxes.MouseXAndY;

    Vector2 rotation = new Vector2(0f, 0f);
    Vector2 smoothMouse;

    public Vector2 sensitivity = new Vector2(15f, 15f);
    public Vector2 clampInDegrees = new Vector2(180f, 60f);
    public Vector2 smoothing = new Vector2(2f, 2f);

    Quaternion originalRotation;

    void Start()
    {
        originalRotation = transform.localRotation;

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        if (axes == RotationAxes.MouseXAndY)
        {

            Vector2 mouseDelta = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
            mouseDelta = Vector2.Scale(mouseDelta, new Vector2(sensitivity.x * smoothing.x, sensitivity.y * smoothing.y));

            smoothMouse.x = Mathf.Lerp(smoothMouse.x, mouseDelta.x, 1f / smoothing.x);
            smoothMouse.y = Mathf.Lerp(smoothMouse.y, mouseDelta.y, 1f / smoothing.y);

            rotation += smoothMouse;

            if (clampInDegrees.x < 360)
            {

                rotation.x = ClampAngle(rotation.x, -clampInDegrees.x * 0.5f, clampInDegrees.x * 0.5f);
            }

            if (clampInDegrees.y < 360)
            {

                rotation.y = ClampAngle(rotation.y, -clampInDegrees.y * 0.5f, clampInDegrees.y * 0.5f);
            }

            Quaternion xQuaternion = Quaternion.AngleAxis(rotation.x, Vector3.up);
            Quaternion yQuaternion = Quaternion.AngleAxis(rotation.y, -Vector3.right);

            transform.localRotation = originalRotation * xQuaternion * yQuaternion;
        }
        else if (axes == RotationAxes.MouseX)
        {

            Vector2 mouseDelta = new Vector2(Input.GetAxisRaw("Mouse X"), 0f);
            mouseDelta = Vector2.Scale(mouseDelta, new Vector2(sensitivity.x * smoothing.x, 0f));

            smoothMouse.x = Mathf.Lerp(smoothMouse.x, mouseDelta.x, 1f / smoothing.x);
            smoothMouse.y = 0f;

            rotation += smoothMouse;

            if (clampInDegrees.x < 360)
            {

                rotation.x = ClampAngle(rotation.x, -clampInDegrees.x * 0.5f, clampInDegrees.x * 0.5f);
            }

            Quaternion xQuaternion = Quaternion.AngleAxis(rotation.x, Vector3.up);
            transform.localRotation = originalRotation * xQuaternion;
        }
        else
        {

            Vector2 mouseDelta = new Vector2(0f, Input.GetAxisRaw("Mouse Y"));
            mouseDelta = Vector2.Scale(mouseDelta, new Vector2(0f, sensitivity.y * smoothing.y));

            smoothMouse.y = Mathf.Lerp(smoothMouse.y, mouseDelta.y, 1f / smoothing.y);

            rotation += smoothMouse;

            if (clampInDegrees.y < 360)
            {

                rotation.y = ClampAngle(rotation.y, -clampInDegrees.y * 0.5f, clampInDegrees.y * 0.5f);
            }

            Quaternion yQuaternion = Quaternion.AngleAxis(rotation.y, -Vector3.right);

            transform.localRotation = originalRotation * yQuaternion;
        }
    }

    public float ClampAngle(float angle, float min, float max)
    {

        if (angle < -360f)
        {

            angle += 360f;
        }

        if (angle > 360f)
        {

            angle -= 360f;
        }

        return Mathf.Clamp(angle, min, max);
    }
}
