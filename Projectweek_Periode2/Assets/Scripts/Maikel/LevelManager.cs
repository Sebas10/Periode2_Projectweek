﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    [SerializeField] private List<GameObject> _aiSpawner = new List<GameObject>();

    [Header("Spawning Variables")]
    [SerializeField] private Transform _player;
    [SerializeField] private Transform _target;
    [SerializeField] private float _spawnDelay = 3f;

    [SerializeField] private GameObject DeathCamera;
    [SerializeField] private GameObject Canvas;
    [SerializeField] private GameObject Canvas1;

    private float Timer = 0f;

    private void Awake()
    {
        for(int i = 0; i < _aiSpawner.Count; i++)
        {
            _aiSpawner[i].GetComponent<SpawningAI>().SetTargetAtStart(_player);
            _aiSpawner[i].GetComponent<SpawningAI>().SetObjectiveAtStart(_target);
            _aiSpawner[i].GetComponent<SpawningAI>().SetSpawnDelay(_spawnDelay);
        }

        DeathCamera.SetActive(false);
        Canvas.SetActive(false);
        Canvas1.SetActive(true);
    }

    private void Update()
    {
        if(_player == null)
        {
            DeathCamera.SetActive(true);
            Canvas.SetActive(true);
            Canvas1.SetActive(false);

            Timer += 1f * Time.deltaTime;

            if(Timer >= 10f)
            {
                SceneManager.LoadScene(0);
            }
        }
    }
}
